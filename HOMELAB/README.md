# Generate lab environment
* Generate ssh keys in keys.  Not recommended to use actual production ssh key.
* Create `variables.tf` with the following:
  * ssh_pub_key (path to public ssh key)
  * ssh_private_key (path to private ssh key)
  * default_pw (default root pw)
  * salt_dir (local path to saltstack configuration)
  * proxmox
    * url
    * node
    * username
    * password
  * internal_dns (Internal IP for DC/ DNS)
  * upstream_dns (Upstream DNS)
  * domain
* Build lab via `terraform apply`
* Once all machines are visible via the salt master, run:
```
salt '*' state.apply ossec
salt '*' state.apply firewall
salt dc* state.apply dc
salt consul* state.apply consul

```

* Manual steps after salt deployments:
  * OSSEC initialization
  * Binding machines to AD
