///////////////////////////////////////////////////////////
// DC1
// Samba server with domain controller role
///////////////////////////////////////////////////////////

resource "proxmox_lxc" "dc1" {

  target_node  = "${var.proxmox[0].node}"
  hostname     = "dc1.${var.domain}"
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  password     = "${var.default_pw}"
  unprivileged = false
  ssh_public_keys = file("${var.ssh_pub_key}")
  vmid = 104

  // Terraform will crash without rootfs defined
  rootfs {
      storage = "local-lvm"
      size    = "8G"
  }

  memory = 512
  swap = 512

  network {
      name    = "eth0"
      bridge  = "vmbr0"
      ip      = "192.168.122.104/24"
      gw      = "192.168.122.1"
  }
  nameserver = "${var.internal_dns} ${var.upstream_dns}"
  searchdomain = "${var.domain}"
  start = true
  onboot = true

}


///////////////////////////////////////////////////////////
// DC1 configuration
///////////////////////////////////////////////////////////
resource "null_resource" "dc1_config" {
  depends_on = [null_resource.debmirror_config]

connection {
    type = "ssh"
    user = "root"
    private_key = file("${var.ssh_private_key}")
    host = split("/", proxmox_lxc.dc1.network[0].ip)[0]
  }

  // Deb Package mirror
  provisioner "file" {
    source      = "files/debmirror/01proxy"
    destination = "/etc/apt/apt.conf.d/01proxy"
  }

  // Salt GPG key
  provisioner "file" {
    source      = "keys/salt-archive-keyring-2023.gpg"
    destination = "/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg"
  }

  // Prepare destination
  provisioner "remote-exec" {
   inline = [
      "echo 'deb [signed-by=/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main' | sudo tee /etc/apt/sources.list.d/salt.list",
      "apt update && apt install -y salt-minion",
      "echo 'master: 192.168.122.102\ngrains:\n  environment: lab\n  roles:\n    - dc\n    - lab' > /etc/salt/minion.d/salt.conf",
      "systemctl restart salt-minion"
    ]
  }

}
