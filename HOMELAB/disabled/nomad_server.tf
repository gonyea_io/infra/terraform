///////////////////////////////////////////////////////////
// Nomad Cluster
///////////////////////////////////////////////////////////
resource "proxmox_lxc" "nomad" {
  count = 3
  depends_on = [null_resource.debmirror_config]
  provider = proxmox
  target_node  = "${var.proxmox[0].node}"
  hostname     = "nomad-${count.index + 1}.lab.corp"
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  password     = "${var.default_pw}"
  unprivileged = false
  ssh_public_keys = file("${var.ssh_pub_key}")
  vmid = count.index + 115

  // Terraform will crash without rootfs defined
  rootfs {
    storage = "local-lvm"
    size    = "12G"
  }

  // Extra features for nesting docker within this lxc
  features {
    nesting = true
  }

  cores = 4
  memory = 4096
  swap = 512

  network {
    name    = "eth0"
    bridge  = "vmbr0"
    ip      = "192.168.122.${count.index + 115}/24"
    gw      = "192.168.122.1"
  }

  start = true
  onboot = true


}


///////////////////////////////////////////////////////////
// Nomad Server configuration
///////////////////////////////////////////////////////////
resource "null_resource" "nomad" {
  count = length(proxmox_lxc.nomad)
  depends_on = [proxmox_lxc.nomad]

  connection {
    type = "ssh"
    user = "root"
    private_key = file("${var.ssh_private_key}")
    host = trimsuffix(element(proxmox_lxc.nomad[*].network[0].ip, count.index), "/24")
  }

  // Deb Package mirror
  provisioner "file" {
    source      = "files/debmirror/01proxy"
    destination = "/etc/apt/apt.conf.d/01proxy"
  }

  // Salt GPG key
  provisioner "file" {
    source      = "keys/salt-archive-keyring-2023.gpg"
    destination = "/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg"
  }

  provisioner "remote-exec" {
   inline = [
      "apt update && apt install -y curl",
      "echo 'deb [signed-by=/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main' | tee /etc/apt/sources.list.d/salt.list",
      "apt update && apt install -y salt-minion",
      "echo 'master: 192.168.122.102\ngrains:\n  environment: lab\n  roles:\n    - dockerRunner\n    - consul\n    - consulClient\n    - nomadServer\n' > /etc/salt/minion.d/salt.conf",
      "systemctl restart salt-minion",
      "apt purge -y ufw"
    ]
  }

}
