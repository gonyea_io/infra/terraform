///////////////////////////////////////////////////////////
// OSSEC
///////////////////////////////////////////////////////////
resource "proxmox_lxc" "ossec" {
  depends_on = [null_resource.debmirror_config]
  provider = proxmox
  target_node  = "${var.proxmox[0].node}"
  hostname     = "ossec.lab.corp"
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  password     = "${var.default_pw}"
  unprivileged = true
  ssh_public_keys = file("${var.ssh_pub_key}")
  vmid = 113

  // Terraform will crash without rootfs defined
  rootfs {
    storage = "local-lvm"
    size    = "8G"
  }

  memory = 1024
  swap = 512

  network {
    name    = "eth0"
    bridge  = "vmbr0"
    ip      = "192.168.122.113/24"
    gw      = "192.168.122.1"
  }

  start = true
  onboot = true

}


///////////////////////////////////////////////////////////
// OSSEC configuration
///////////////////////////////////////////////////////////
resource "null_resource" "ossec" {
  depends_on = [proxmox_lxc.ossec]
  connection {
    type = "ssh"
    user = "root"
    private_key = file("${var.ssh_private_key}")
    host = trimsuffix(proxmox_lxc.ossec.network[0].ip, "/24")
  }

  // Deb Package mirror
  provisioner "file" {
    source      = "files/debmirror/01proxy"
    destination = "/etc/apt/apt.conf.d/01proxy"
  }

  // Salt GPG key
  provisioner "file" {
    source      = "keys/salt-archive-keyring-2023.gpg"
    destination = "/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg"
  }

  provisioner "remote-exec" {
   inline = [
      "apt update && apt install -y curl",
      "echo 'deb [signed-by=/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main' | sudo tee /etc/apt/sources.list.d/salt.list",
      "apt update && apt install -y salt-minion",
      "echo 'master: 192.168.122.102\ngrains:\n  environment: lab\n  roles:\n    - ossecServer\n' > /etc/salt/minion.d/salt.conf",
      "systemctl restart salt-minion",
      "apt purge -y ufw"
    ]
  }

}