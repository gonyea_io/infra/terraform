///////////////////////////////////////////////////////////
// Graylog
///////////////////////////////////////////////////////////
resource "proxmox_lxc" "graylog" {
  depends_on = [null_resource.debmirror_config]
  provider = proxmox
  target_node  = "${var.proxmox[0].node}"
  hostname     = "graylog.lab.corp"
  ostemplate   = "local:vztmpl/ubuntu-22.04-standard_22.04-1_amd64.tar.zst"
  password     = "${var.default_pw}"
  unprivileged = true
  ssh_public_keys = file("${var.ssh_pub_key}")
  vmid = 114

  // Terraform will crash without rootfs defined
  rootfs {
    storage = "local-lvm"
    size    = "24G"
  }

  cores = 4
  memory = 8192
  swap = 512

  network {
    name    = "eth0"
    bridge  = "vmbr0"
    ip      = "192.168.122.114/24"
    gw      = "192.168.122.1"
  }

  start = true
  onboot = true

}


///////////////////////////////////////////////////////////
// Graylog configuration
///////////////////////////////////////////////////////////
resource "null_resource" "graylog" {
  depends_on = [proxmox_lxc.graylog]
  connection {
    type = "ssh"
    user = "root"
    private_key = file("${var.ssh_private_key}")
    host = trimsuffix(proxmox_lxc.graylog.network[0].ip, "/24")
  }

  // Deb Package mirror
  provisioner "file" {
    source      = "files/debmirror/01proxy"
    destination = "/etc/apt/apt.conf.d/01proxy"
  }

  // Salt GPG key
  provisioner "file" {
    source      = "keys/salt-archive-keyring-2023.gpg"
    destination = "/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg"
  }

  provisioner "remote-exec" {
   inline = [
      "apt update && apt install -y curl",
      "echo 'deb [signed-by=/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main' | tee /etc/apt/sources.list.d/salt.list",
      "apt update && apt install -y salt-minion",
      "echo 'master: 192.168.122.102\ngrains:\n  environment: lab\n  roles:\n    - graylog\n' > /etc/salt/minion.d/salt.conf",
      "systemctl restart salt-minion",
      "apt purge -y ufw"
    ]
  }

}
