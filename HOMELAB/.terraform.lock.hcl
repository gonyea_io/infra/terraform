# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/cyrilgdn/postgresql" {
  version     = "1.19.0"
  constraints = "1.19.0"
  hashes = [
    "h1:TnyNonQsf+M7uOP9lxHGymiLP1VE5mPIMEsk8FspD8k=",
    "zh:00f946e9e657556234b1ab6b11ff94c1f67d2dec20a0643f5f70fd4b612167e4",
    "zh:01cac73ff59c3ee3cc3716b006266a81d182f2622e944c08081551a2688ee5bb",
    "zh:0e688a1c326baff9fb3e70bed4911b13220338045bfc77796f2468699e9f715b",
    "zh:12f8bb65b891356705c7ece166611a6096ffe002a78af8254f946b1fd7f5aaef",
    "zh:2f2a7099f50f14b4a6026a3db7e3de01c50efa855b497e0c03d291b72ee85e08",
    "zh:8f41731313a56d04b01c730059b463df32e5cb8cc9eace8e579650a55dd9cfb1",
    "zh:b04fe91735b24148aaa438cfdc33ac8e31b92b61ed2cb59ca62f3709396af619",
    "zh:b09302fe09e385f277ac8ea1d70adc7acbea623f04e4c362f36599c287c1e92c",
    "zh:b2fa7e8a135b69b5c204ce9e6be9a1219c3550c0e4f4bb59029f79e2a7639163",
    "zh:c8d92e962da3a4f620aad86b8bd1f2f373670f0d3b62f8e7d8efb31b73014d35",
    "zh:d2c3070a6ae03aa71156ed5ff8d68bf7c9cf402ff540d5baf5f5a9915c70a85d",
    "zh:f39a3a9c3be75630b69efc5ee233abb89b5d8ffab36318074750d58da5aab612",
    "zh:f8195be4222937ac15a3c4a4cfeacc87627a550bf257bc2430d2866b9784afb6",
    "zh:ff2dcedfb3fb8f9df5cc35ac0388dae5d1fc0c5d90db81b67414a7ec179abd98",
  ]
}

provider "registry.opentofu.org/hashicorp/external" {
  version     = "2.3.1"
  constraints = "2.3.1"
  hashes = [
    "h1:FeQjc5N8zcTydteYqzZmQYTC/6CWvCLb4od7J4gRn+g=",
    "zh:25e28cd82338279d665534be8d40af29ab7078661985043bf9a86fa7dc87b0f4",
    "zh:4715643dc51b9694d480a38794eb749622abdf1e33e7ce1f31fa6c72b4db8de1",
    "zh:4e2540049dc981af022dbc99f105d04d842a087ca7887b75f916fa4dd5a09647",
    "zh:52a5992f37415bab18adeee0a77ae5d54cbe655fbcfe4d229272c882cfa82c53",
    "zh:56444b0128fc680c6cb9db6c5a6267a932522d0b842d02a1d1546f55bb70eb25",
    "zh:584df0048d5e6f3b2d62dd0d6ee63e45a2b31c3462d9861da61f3cedfb021734",
    "zh:6221bfd2f0381c1b9395992b1811fc030fd930aa4119246ec9877cc7857cf424",
    "zh:62af0ffaadfc64530362b5f01ab64559402729064bd2c6204533c3eb46849fb6",
    "zh:aaebb9c4818f9b5ce193661171c1b60e4387e83564f7bbe2fc66845023d74720",
    "zh:da8b076edb16fa0a23bd84e9156bf165459ef621a4a8327f3eae51d21f053c61",
  ]
}

provider "registry.opentofu.org/hashicorp/null" {
  version     = "3.2.1"
  constraints = "3.2.1"
  hashes = [
    "h1:uQv2oPjJv+ue8bPrVp+So2hHd90UTssnCNajTW554Cw=",
    "zh:40335019c11e5bdb3780301da5197cbc756b4b5ac3d699c52583f1d34e963176",
    "zh:42356e687656fc8ec1f230f786f830f344e64419552ec483e2bc79bd4b7cf1e8",
    "zh:5ce03460813954cbebc9f9ad5befbe364d9dc67acb08869f67c1aa634fbf6d6c",
    "zh:658ea3e3e7ecc964bdbd08ecde63f3d79f298bab9922b29a6526ba941a4d403a",
    "zh:68c06703bc57f9c882bfedda6f3047775f0d367093d00efb040800c798b8a613",
    "zh:80fd03335f793dc54302dd53da98c91fd94f182bcacf13457bed1a99ecffbc1a",
    "zh:91a76f371815a130735c8fcb6196804d878aebcc67b4c3b73571d2063336ffd8",
    "zh:c146fc0291b7f6284fe4d54ce6aaece6957e9acc93fc572dd505dfd8bcad3e6c",
    "zh:c38c9a295cfae9fb6372523c34b9466cd439d5e2c909b56a788960d387c24320",
    "zh:e25265d4e87821d18dc9653a0ce01978a1ae5d363bc01dd273454db1aa0309c7",
  ]
}

provider "registry.opentofu.org/kreuzwerker/docker" {
  version     = "3.0.1"
  constraints = "3.0.1"
  hashes = [
    "h1:X2wZHQoG54NmtojeFcX0PSJPelaIejQRqyyI2h+LjWg=",
    "zh:02f60126ca16b344092df3c315296bf1a216c3b2a68eddb3c89fdfa5ea826118",
    "zh:0d2ee9624a54dbc10538b0c4e296348641b9bfba1354b3f872e43f7ec69a75f2",
    "zh:473d7427da8c9efc231266abc7fdc27fca5f9ee0bdfcdb9914f0a2886e3e23b8",
    "zh:5f0189bcd0c944c001098cb17a23efa79df8f0eec8644a64fe0e4200983ba5b7",
    "zh:6200319c41d6baad3f46701a4028412f8ae2496e29fc4fef9584cc71da5fbbe6",
    "zh:650be621f2216b1240f148eae8fcf80ec57c35925e2b212db7c23a70b9e67e06",
    "zh:72fcfa6207251105066a34f0ec6d27ecc658b565e84fa946da376dd1afadd265",
    "zh:92fc352a2090d3d380c7c8e8bbdf6f99d93a0182701056bb1d2dbfd5049e8ca6",
    "zh:a7e2ef666c2a7eb5661b06cfbd7635cb9543524e7bf6a3851dcf6eacc9950cc4",
    "zh:a8604595e61e8919c51a8656800c8c64557f9a2bc00309315895b380f2e9be19",
    "zh:caf65603a84b749d8f3af2ee47b66f7e21d481f981e2e1d1d59838751c5e3be4",
    "zh:dad40c4e57da284e7f57b5c0cc9dfac3cb27b01d2f2436fbe3464f0a2111b262",
    "zh:dc1b173dbcba9d74879b16f36f6d9e97ef62fbd6fca8db79ec4fe4ec69c0e2f3",
    "zh:e506d04677383b6d62bd69d42dc9005e27a45ccc2efc6e0de607e1f8445981d2",
  ]
}

provider "registry.opentofu.org/telmate/proxmox" {
  version     = "2.9.13"
  constraints = "2.9.13"
  hashes = [
    "h1:u9yr+S7Nu+Qtwdxq3bD/Pl6fSPGq8WRHbIBtGuzU/SQ=",
    "zh:1df5140fc89321608f53ce6a7dee284f3893eb4e4fa15344e2baa98c24809c78",
    "zh:353436d422e29d280d501c37fdf3d19abc897ea8cfcbc0045ed30bd5fa69f7e0",
    "zh:4d711ed6bc2f86684bddb644e624313361053cd7f0fe2aa9be15495723644ec8",
    "zh:53cccbd339e2fefc3b812d2971b51a136b68bdac66633990a941f6ec33a55da5",
    "zh:53f6bcc332b201a774d8881196bb43392cd463ff553fbd844034b9d69e18364f",
    "zh:7f6a2c856c65a9cba6f7e411c6f58c36244a9d69a481bb77cbf6ed21df1d92a0",
    "zh:948ad6b94518ab79aa63c34e07e4a00c12779dbb85cbc3bd4506537e2c7db5b0",
    "zh:9c0c689c734e2d1d1325b4c7fc86f667d9d099b67538529261b06da80fec5355",
    "zh:be9db6f91724582d0a5f0ff8c8e24f40b5de6467ff6a5bbafafb80a985b23385",
    "zh:f042efe9634162a3aa1960c76f02246c7e8109046c81940eaa32c4f2fbbb33f6",
    "zh:f2ab546b96a3fdb8a184153eaead690fd978c749dfa4eaf2cf634fa5802dfe1f",
    "zh:fab6c224e8a8a756503b25382c4b9c29a3f2e55a4f034166f9e99fa2fdacd676",
    "zh:fdd9ef52498010a5758470102846e250e80790cf869d723489d7f7e373df0d87",
    "zh:fe32c44aebe6bde857dae268a1153ec6eed23407508c588f34a53205c7a4d430",
  ]
}
