///////////////////////////////////////////////////////////
// Salt master
///////////////////////////////////////////////////////////
resource "null_resource" "final" {
  # Used to do salt steps last.
  depends_on = [null_resource.ossec]
}

resource "proxmox_lxc" "saltmaster" {
  depends_on = [null_resource.final]
  provider = proxmox
  target_node  = "${var.proxmox[0].node}"
  hostname     = "salt.${var.domain}"
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  password     = "${var.default_pw}"
  unprivileged = true
  ssh_public_keys = file("${var.ssh_pub_key}")
  vmid = 102

  // Terraform will crash without rootfs defined
  rootfs {
    storage = "local-lvm"
    size    = "8G"
  }

  memory = 1024
  swap = 512

  network {
    name    = "eth0"
    bridge  = "vmbr0"
    ip      = "192.168.122.102/24"
    gw      = "192.168.122.1"
  }
  nameserver = "${var.internal_dns} ${var.upstream_dns}"
  searchdomain = "${var.domain}"
  start = true
  onboot = true

}

///////////////////////////////////////////////////////////
// Salt master configuration
///////////////////////////////////////////////////////////
resource "null_resource" "saltmaster_config" {
  depends_on = [proxmox_lxc.saltmaster]

  connection {
    type = "ssh"
    user = "root"
    private_key = file("${var.ssh_private_key}")
    host = split("/", proxmox_lxc.saltmaster.network[0].ip)[0]
  }

  // Deb Package mirror
  provisioner "file" {
    source      = "files/debmirror/01proxy"
    destination = "/etc/apt/apt.conf.d/01proxy"
  }

  // Salt GPG key
  provisioner "file" {
    source      = "keys/salt-archive-keyring-2023.gpg"
    destination = "/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg"
  }

  provisioner "remote-exec" {
   inline = [
      "mkdir -p /srv/",
      "apt update && apt install -y curl",
      "echo 'deb [signed-by=/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main' | sudo tee /etc/apt/sources.list.d/salt.list",
      "apt update && apt install -y git salt-master salt-minion",
      "echo 'grains:\n  environment: lab\n  roles:\n    - saltmaster\n' > /etc/salt/minion.d/salt.conf",
      "systemctl restart salt-minion salt-master",
    ]
  }

  // Transfer saltstack configuration
  provisioner "file" {
    source      = "${var.salt_dir}/"
    destination = "/srv"
  }

}
