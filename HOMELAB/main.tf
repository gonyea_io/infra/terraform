terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "3.0.1"
    }
    external = {
      source = "hashicorp/external"
      version = "2.3.1"
    }
    null = {
      source = "hashicorp/null"
      version = "3.2.1"
    }
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.13"
    }
    postgresql = {
      source = "cyrilgdn/postgresql"
      version = "1.19.0"
    }
  }
}
provider "proxmox" {
  pm_api_url = "${var.proxmox[0].url}"
  pm_user = "${var.proxmox[0].username}"
  pm_password = "${var.proxmox[0].password}"
}
