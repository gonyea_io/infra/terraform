///////////////////////////////////////////////////////////
// Debmirror
// Acts as a debian package repository mirror/ cache
///////////////////////////////////////////////////////////

resource "proxmox_lxc" "debmirror" {

  target_node  = "${var.proxmox[0].node}"
  hostname     = "debmirror.${var.domain}"
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  password     = "${var.default_pw}"
  unprivileged = true
  ssh_public_keys = file("${var.ssh_pub_key}")
  vmid = 103

  // Terraform will crash without rootfs defined
  rootfs {
      storage = "local-lvm"
      size    = "8G"
  }

  memory = 512
  swap = 512

  network {
      name    = "eth0"
      bridge  = "vmbr0"
      ip      = "192.168.122.103/24"
      gw      = "192.168.122.1"
  }
  nameserver = "${var.internal_dns} ${var.upstream_dns}"
  searchdomain = "${var.domain}"
  start = true
  onboot = true

}


///////////////////////////////////////////////////////////
// Debmirror configuration
///////////////////////////////////////////////////////////
resource "null_resource" "debmirror_config" {
  depends_on = [proxmox_lxc.debmirror]

connection {
    type = "ssh"
    user = "root"
    private_key = file("${var.ssh_private_key}")
    host = split("/", proxmox_lxc.debmirror.network[0].ip)[0]
  }

  // Salt GPG key
  provisioner "file" {
    source      = "keys/salt-archive-keyring-2023.gpg"
    destination = "/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg"
  }

  // Prepare destination
  provisioner "remote-exec" {
   inline = [
      "echo 'deb [signed-by=/etc/apt/trusted.gpg.d/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main' | sudo tee /etc/apt/sources.list.d/salt.list",
      "echo 'apt-cacher apt-cacher/mode select daemon' | debconf-set-selections -v",
      "apt update && apt install -y salt-minion apache2 apt-cacher",
      "echo 'master: 192.168.122.102\ngrains:\n  environment: lab\n  roles:\n    - debmirror\n' > /etc/salt/minion.d/salt.conf",
      "systemctl restart salt-minion"
    ]
  }

  provisioner "file" {
    source      = "files/debmirror/apt-cacher.conf"
    destination = "/etc/apt-cacher/apt-cacher.conf"
  }

  provisioner "remote-exec" {
   inline = [
      "systemctl restart apache2 apt-cacher"
    ]
  }
}
