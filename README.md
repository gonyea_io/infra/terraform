# Gonyea.io - Terraforms

## Homelab Setup
* Change directory to the lab environment
* Generate a new ssh keypair in the keys folder
* Generate a `variables.tf` file to store the following items:
  * pm_user - User for Proxmox
  * pm_password - Password for proxmox
  * pm_node - Proxmox node to build new resources in
  * ssh_pub_key - path to ssh pubkey
  * ssh_private_key - path to ssh privatekey
  * default_pw - default pw to apply to the root user on newly built resources
  * salt_dir - local path to saltstack repository
* Run `tofu apply`

## UAT Setup

## Production Setup
